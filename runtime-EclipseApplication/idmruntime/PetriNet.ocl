import 'PetriNet.ecore'

package petrinet

context PetriNet
inv Unique_name ('Non-unique name.'):
	self.nodes
	->isUnique(wd | wd.name)
		
context Node
--
-- example invariant with a custom error message to verify that
-- the 'name' property of all 'petrinet::Node' instances is non-null
--
inv NonNull_name('The \'name\' property of "' + self.toString() + '" is null'):
	name <> null

def: getParentPetriNet(): PetriNet =
	PetriNet.allInstances()
		->select(p | p.nodes->includes(self))
		->asSequence()->first()
		
inv Unique_name ('Non-unique name: there is more than one "'+ self.name+'".'):
	self.getParentPetriNet().nodes
	->isUnique(wd | wd.name)
	
context Arc
inv AlternancePlaceTransition('An arc source kind can\'t be the same as the target.'):
	(
		(
			self.source.oclIsTypeOf(Place)
				and 
			self.target.oclIsTypeOf(Transition)
		)
			or
		(
			self.source.oclIsTypeOf(Transition)
				 and 
			self.target.oclIsTypeOf(Place)
		)
	)

inv OnlyPositiveToken(self.source.name + ' -> '+self.target.name +' have '+self.tokenNb.toString()+' tokens. Must be >= 0.'):
	self.tokenNb >= 1

context Place
inv OnlyPositiveToken(self.name+' have '+self.tokenNb.toString()+' tokens. Must be >= 0.'):
	self.tokenNb >= 0
endpackage