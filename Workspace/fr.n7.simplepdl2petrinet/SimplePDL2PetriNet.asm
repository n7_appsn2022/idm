<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm version="1.0" name="0">
	<cp>
		<constant value="SimplePDL2PetriNet"/>
		<constant value="links"/>
		<constant value="NTransientLinkSet;"/>
		<constant value="col"/>
		<constant value="J"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="OclParametrizedType"/>
		<constant value="#native"/>
		<constant value="Collection"/>
		<constant value="J.setName(S):V"/>
		<constant value="OclSimpleType"/>
		<constant value="OclAny"/>
		<constant value="J.setElementType(J):V"/>
		<constant value="TransientLinkSet"/>
		<constant value="A.__matcher__():V"/>
		<constant value="A.__exec__():V"/>
		<constant value="self"/>
		<constant value="__resolve__"/>
		<constant value="1"/>
		<constant value="J.oclIsKindOf(J):B"/>
		<constant value="18"/>
		<constant value="NTransientLinkSet;.getLinkBySourceElement(S):QNTransientLink;"/>
		<constant value="J.oclIsUndefined():B"/>
		<constant value="15"/>
		<constant value="NTransientLink;.getTargetFromSource(J):J"/>
		<constant value="17"/>
		<constant value="30"/>
		<constant value="Sequence"/>
		<constant value="2"/>
		<constant value="A.__resolve__(J):J"/>
		<constant value="QJ.including(J):QJ"/>
		<constant value="QJ.flatten():QJ"/>
		<constant value="e"/>
		<constant value="value"/>
		<constant value="resolveTemp"/>
		<constant value="S"/>
		<constant value="NTransientLink;.getNamedTargetFromSource(JS):J"/>
		<constant value="name"/>
		<constant value="__matcher__"/>
		<constant value="A.__matchProcess2PetriNet():V"/>
		<constant value="A.__matchWorkDefinition2PetriNet():V"/>
		<constant value="A.__matchWorkSequence2PetriNet():V"/>
		<constant value="__exec__"/>
		<constant value="Process2PetriNet"/>
		<constant value="NTransientLinkSet;.getLinksByRule(S):QNTransientLink;"/>
		<constant value="A.__applyProcess2PetriNet(NTransientLink;):V"/>
		<constant value="WorkDefinition2PetriNet"/>
		<constant value="A.__applyWorkDefinition2PetriNet(NTransientLink;):V"/>
		<constant value="WorkSequence2PetriNet"/>
		<constant value="A.__applyWorkSequence2PetriNet(NTransientLink;):V"/>
		<constant value="getProcess"/>
		<constant value="Msimplepdl!ProcessElement;"/>
		<constant value="Process"/>
		<constant value="simplepdl"/>
		<constant value="J.allInstances():J"/>
		<constant value="processElements"/>
		<constant value="0"/>
		<constant value="J.includes(J):J"/>
		<constant value="B.not():B"/>
		<constant value="CJ.including(J):CJ"/>
		<constant value="J.asSequence():J"/>
		<constant value="J.first():J"/>
		<constant value="9:2-9:19"/>
		<constant value="9:2-9:34"/>
		<constant value="10:16-10:17"/>
		<constant value="10:16-10:33"/>
		<constant value="10:44-10:48"/>
		<constant value="10:16-10:49"/>
		<constant value="9:2-10:50"/>
		<constant value="9:2-11:17"/>
		<constant value="9:2-11:26"/>
		<constant value="p"/>
		<constant value="__matchProcess2PetriNet"/>
		<constant value="IN"/>
		<constant value="MMOF!Classifier;.allInstancesFrom(S):QJ"/>
		<constant value="TransientLink"/>
		<constant value="NTransientLink;.setRule(MATL!Rule;):V"/>
		<constant value="NTransientLink;.addSourceElement(SJ):V"/>
		<constant value="pn"/>
		<constant value="PetriNet"/>
		<constant value="petrinet"/>
		<constant value="NTransientLink;.addTargetElement(SJ):V"/>
		<constant value="NTransientLinkSet;.addLink2(NTransientLink;B):V"/>
		<constant value="16:5-18:55"/>
		<constant value="__applyProcess2PetriNet"/>
		<constant value="NTransientLink;"/>
		<constant value="NTransientLink;.getSourceElement(S):J"/>
		<constant value="NTransientLink;.getTargetElement(S):J"/>
		<constant value="3"/>
		<constant value="Node"/>
		<constant value="petriNet"/>
		<constant value="nodes"/>
		<constant value="Arc"/>
		<constant value="arcs"/>
		<constant value="16:36-16:37"/>
		<constant value="16:36-16:42"/>
		<constant value="16:28-16:42"/>
		<constant value="17:12-17:25"/>
		<constant value="17:12-17:40"/>
		<constant value="17:12-17:56"/>
		<constant value="17:3-17:56"/>
		<constant value="18:11-18:23"/>
		<constant value="18:11-18:38"/>
		<constant value="18:11-18:54"/>
		<constant value="18:3-18:54"/>
		<constant value="link"/>
		<constant value="__matchWorkDefinition2PetriNet"/>
		<constant value="WorkDefinition"/>
		<constant value="wd"/>
		<constant value="p_idle"/>
		<constant value="Place"/>
		<constant value="p_inProgress"/>
		<constant value="p_started"/>
		<constant value="p_finished"/>
		<constant value="t_start"/>
		<constant value="Transition"/>
		<constant value="t_finish"/>
		<constant value="a_idleToinProgress"/>
		<constant value="a_inProgress"/>
		<constant value="a_started"/>
		<constant value="a_inProgressToFinish"/>
		<constant value="a_finished"/>
		<constant value="26:3-28:18"/>
		<constant value="30:3-31:37"/>
		<constant value="33:3-34:34"/>
		<constant value="36:3-37:35"/>
		<constant value="40:3-40:58"/>
		<constant value="42:3-42:60"/>
		<constant value="46:3-48:30"/>
		<constant value="49:3-51:35"/>
		<constant value="52:3-54:32"/>
		<constant value="55:3-57:31"/>
		<constant value="58:3-60:33"/>
		<constant value="__applyWorkDefinition2PetriNet"/>
		<constant value="4"/>
		<constant value="5"/>
		<constant value="6"/>
		<constant value="7"/>
		<constant value="8"/>
		<constant value="9"/>
		<constant value="10"/>
		<constant value="11"/>
		<constant value="12"/>
		<constant value="13"/>
		<constant value="_idle"/>
		<constant value="J.+(J):J"/>
		<constant value="tokenNb"/>
		<constant value="_inprogress"/>
		<constant value="_started"/>
		<constant value="_finished"/>
		<constant value="_start"/>
		<constant value="_finish"/>
		<constant value="source"/>
		<constant value="target"/>
		<constant value="27:13-27:15"/>
		<constant value="27:13-27:20"/>
		<constant value="27:23-27:30"/>
		<constant value="27:13-27:30"/>
		<constant value="27:5-27:30"/>
		<constant value="28:16-28:17"/>
		<constant value="28:5-28:17"/>
		<constant value="31:13-31:15"/>
		<constant value="31:13-31:20"/>
		<constant value="31:23-31:36"/>
		<constant value="31:13-31:36"/>
		<constant value="31:5-31:36"/>
		<constant value="34:13-34:15"/>
		<constant value="34:13-34:20"/>
		<constant value="34:23-34:33"/>
		<constant value="34:13-34:33"/>
		<constant value="34:5-34:33"/>
		<constant value="37:13-37:15"/>
		<constant value="37:13-37:20"/>
		<constant value="37:23-37:34"/>
		<constant value="37:13-37:34"/>
		<constant value="37:5-37:34"/>
		<constant value="40:41-40:43"/>
		<constant value="40:41-40:48"/>
		<constant value="40:49-40:57"/>
		<constant value="40:41-40:57"/>
		<constant value="40:33-40:57"/>
		<constant value="42:42-42:44"/>
		<constant value="42:42-42:49"/>
		<constant value="42:50-42:59"/>
		<constant value="42:42-42:59"/>
		<constant value="42:34-42:59"/>
		<constant value="46:49-46:50"/>
		<constant value="46:38-46:50"/>
		<constant value="47:22-47:28"/>
		<constant value="47:12-47:28"/>
		<constant value="48:22-48:29"/>
		<constant value="48:12-48:29"/>
		<constant value="49:43-49:44"/>
		<constant value="49:32-49:44"/>
		<constant value="50:22-50:29"/>
		<constant value="50:12-50:29"/>
		<constant value="51:22-51:34"/>
		<constant value="51:12-51:34"/>
		<constant value="52:40-52:41"/>
		<constant value="52:29-52:41"/>
		<constant value="53:22-53:29"/>
		<constant value="53:12-53:29"/>
		<constant value="54:22-54:31"/>
		<constant value="54:12-54:31"/>
		<constant value="55:51-55:52"/>
		<constant value="55:40-55:52"/>
		<constant value="56:22-56:31"/>
		<constant value="56:12-56:31"/>
		<constant value="57:22-57:30"/>
		<constant value="57:12-57:30"/>
		<constant value="58:41-58:42"/>
		<constant value="58:30-58:42"/>
		<constant value="59:22-59:30"/>
		<constant value="59:12-59:30"/>
		<constant value="60:22-60:32"/>
		<constant value="60:12-60:32"/>
		<constant value="__matchWorkSequence2PetriNet"/>
		<constant value="WorkSequence"/>
		<constant value="ws"/>
		<constant value="a_ws"/>
		<constant value="68:3-79:7"/>
		<constant value="__applyWorkSequence2PetriNet"/>
		<constant value="predecessor"/>
		<constant value="linkType"/>
		<constant value="EnumLiteral"/>
		<constant value="finishToFinish"/>
		<constant value="J.=(J):J"/>
		<constant value="finishToStart"/>
		<constant value="J.or(J):J"/>
		<constant value="41"/>
		<constant value="42"/>
		<constant value="J.resolveTemp(JJ):J"/>
		<constant value="successor"/>
		<constant value="startToStart"/>
		<constant value="72"/>
		<constant value="73"/>
		<constant value="68:34-68:35"/>
		<constant value="68:23-68:35"/>
		<constant value="69:15-69:25"/>
		<constant value="69:38-69:40"/>
		<constant value="69:38-69:52"/>
		<constant value="70:10-70:12"/>
		<constant value="70:10-70:21"/>
		<constant value="70:24-70:39"/>
		<constant value="70:10-70:39"/>
		<constant value="70:45-70:47"/>
		<constant value="70:45-70:56"/>
		<constant value="70:59-70:73"/>
		<constant value="70:45-70:73"/>
		<constant value="70:9-70:74"/>
		<constant value="72:11-72:22"/>
		<constant value="71:11-71:23"/>
		<constant value="70:5-73:11"/>
		<constant value="69:15-73:12"/>
		<constant value="69:5-73:12"/>
		<constant value="74:15-74:25"/>
		<constant value="74:38-74:40"/>
		<constant value="74:38-74:50"/>
		<constant value="75:10-75:12"/>
		<constant value="75:10-75:21"/>
		<constant value="75:24-75:38"/>
		<constant value="75:10-75:38"/>
		<constant value="75:44-75:46"/>
		<constant value="75:44-75:55"/>
		<constant value="75:58-75:71"/>
		<constant value="75:44-75:71"/>
		<constant value="75:9-75:72"/>
		<constant value="77:11-77:21"/>
		<constant value="76:11-76:20"/>
		<constant value="75:5-78:11"/>
		<constant value="74:15-78:12"/>
		<constant value="74:5-78:12"/>
	</cp>
	<field name="1" type="2"/>
	<field name="3" type="4"/>
	<operation name="5">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<push arg="7"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="9"/>
			<pcall arg="10"/>
			<dup/>
			<push arg="11"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="12"/>
			<pcall arg="10"/>
			<pcall arg="13"/>
			<set arg="3"/>
			<getasm/>
			<push arg="14"/>
			<push arg="8"/>
			<new/>
			<set arg="1"/>
			<getasm/>
			<pcall arg="15"/>
			<getasm/>
			<pcall arg="16"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="24"/>
		</localvariabletable>
	</operation>
	<operation name="18">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="4"/>
		</parameters>
		<code>
			<load arg="19"/>
			<getasm/>
			<get arg="3"/>
			<call arg="20"/>
			<if arg="21"/>
			<getasm/>
			<get arg="1"/>
			<load arg="19"/>
			<call arg="22"/>
			<dup/>
			<call arg="23"/>
			<if arg="24"/>
			<load arg="19"/>
			<call arg="25"/>
			<goto arg="26"/>
			<pop/>
			<load arg="19"/>
			<goto arg="27"/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<load arg="19"/>
			<iterate/>
			<store arg="29"/>
			<getasm/>
			<load arg="29"/>
			<call arg="30"/>
			<call arg="31"/>
			<enditerate/>
			<call arg="32"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="33" begin="23" end="27"/>
			<lve slot="0" name="17" begin="0" end="29"/>
			<lve slot="1" name="34" begin="0" end="29"/>
		</localvariabletable>
	</operation>
	<operation name="35">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="4"/>
			<parameter name="29" type="36"/>
		</parameters>
		<code>
			<getasm/>
			<get arg="1"/>
			<load arg="19"/>
			<call arg="22"/>
			<load arg="19"/>
			<load arg="29"/>
			<call arg="37"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="6"/>
			<lve slot="1" name="34" begin="0" end="6"/>
			<lve slot="2" name="38" begin="0" end="6"/>
		</localvariabletable>
	</operation>
	<operation name="39">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<pcall arg="40"/>
			<getasm/>
			<pcall arg="41"/>
			<getasm/>
			<pcall arg="42"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="5"/>
		</localvariabletable>
	</operation>
	<operation name="43">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<get arg="1"/>
			<push arg="44"/>
			<call arg="45"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<load arg="19"/>
			<pcall arg="46"/>
			<enditerate/>
			<getasm/>
			<get arg="1"/>
			<push arg="47"/>
			<call arg="45"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<load arg="19"/>
			<pcall arg="48"/>
			<enditerate/>
			<getasm/>
			<get arg="1"/>
			<push arg="49"/>
			<call arg="45"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<load arg="19"/>
			<pcall arg="50"/>
			<enditerate/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="33" begin="5" end="8"/>
			<lve slot="1" name="33" begin="15" end="18"/>
			<lve slot="1" name="33" begin="25" end="28"/>
			<lve slot="0" name="17" begin="0" end="29"/>
		</localvariabletable>
	</operation>
	<operation name="51">
		<context type="52"/>
		<parameters>
		</parameters>
		<code>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<push arg="53"/>
			<push arg="54"/>
			<findme/>
			<call arg="55"/>
			<iterate/>
			<store arg="19"/>
			<load arg="19"/>
			<get arg="56"/>
			<load arg="57"/>
			<call arg="58"/>
			<call arg="59"/>
			<if arg="26"/>
			<load arg="19"/>
			<call arg="60"/>
			<enditerate/>
			<call arg="61"/>
			<call arg="62"/>
		</code>
		<linenumbertable>
			<lne id="63" begin="3" end="5"/>
			<lne id="64" begin="3" end="6"/>
			<lne id="65" begin="9" end="9"/>
			<lne id="66" begin="9" end="10"/>
			<lne id="67" begin="11" end="11"/>
			<lne id="68" begin="9" end="12"/>
			<lne id="69" begin="0" end="17"/>
			<lne id="70" begin="0" end="18"/>
			<lne id="71" begin="0" end="19"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="72" begin="8" end="16"/>
			<lve slot="0" name="17" begin="0" end="19"/>
		</localvariabletable>
	</operation>
	<operation name="73">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<push arg="53"/>
			<push arg="54"/>
			<findme/>
			<push arg="74"/>
			<call arg="75"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<get arg="1"/>
			<push arg="76"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="44"/>
			<pcall arg="77"/>
			<dup/>
			<push arg="72"/>
			<load arg="19"/>
			<pcall arg="78"/>
			<dup/>
			<push arg="79"/>
			<push arg="80"/>
			<push arg="81"/>
			<new/>
			<pcall arg="82"/>
			<pusht/>
			<pcall arg="83"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="84" begin="19" end="24"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="72" begin="6" end="26"/>
			<lve slot="0" name="17" begin="0" end="27"/>
		</localvariabletable>
	</operation>
	<operation name="85">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="86"/>
		</parameters>
		<code>
			<load arg="19"/>
			<push arg="72"/>
			<call arg="87"/>
			<store arg="29"/>
			<load arg="19"/>
			<push arg="79"/>
			<call arg="88"/>
			<store arg="89"/>
			<load arg="89"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="38"/>
			<call arg="30"/>
			<set arg="38"/>
			<dup/>
			<getasm/>
			<push arg="90"/>
			<push arg="91"/>
			<findme/>
			<call arg="55"/>
			<call arg="61"/>
			<call arg="30"/>
			<set arg="92"/>
			<dup/>
			<getasm/>
			<push arg="93"/>
			<push arg="91"/>
			<findme/>
			<call arg="55"/>
			<call arg="61"/>
			<call arg="30"/>
			<set arg="94"/>
			<pop/>
		</code>
		<linenumbertable>
			<lne id="95" begin="11" end="11"/>
			<lne id="96" begin="11" end="12"/>
			<lne id="97" begin="9" end="14"/>
			<lne id="98" begin="17" end="19"/>
			<lne id="99" begin="17" end="20"/>
			<lne id="100" begin="17" end="21"/>
			<lne id="101" begin="15" end="23"/>
			<lne id="102" begin="26" end="28"/>
			<lne id="103" begin="26" end="29"/>
			<lne id="104" begin="26" end="30"/>
			<lne id="105" begin="24" end="32"/>
			<lne id="84" begin="8" end="33"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="3" name="79" begin="7" end="33"/>
			<lve slot="2" name="72" begin="3" end="33"/>
			<lve slot="0" name="17" begin="0" end="33"/>
			<lve slot="1" name="106" begin="0" end="33"/>
		</localvariabletable>
	</operation>
	<operation name="107">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<push arg="108"/>
			<push arg="54"/>
			<findme/>
			<push arg="74"/>
			<call arg="75"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<get arg="1"/>
			<push arg="76"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="47"/>
			<pcall arg="77"/>
			<dup/>
			<push arg="109"/>
			<load arg="19"/>
			<pcall arg="78"/>
			<dup/>
			<push arg="110"/>
			<push arg="111"/>
			<push arg="81"/>
			<new/>
			<pcall arg="82"/>
			<dup/>
			<push arg="112"/>
			<push arg="111"/>
			<push arg="81"/>
			<new/>
			<pcall arg="82"/>
			<dup/>
			<push arg="113"/>
			<push arg="111"/>
			<push arg="81"/>
			<new/>
			<pcall arg="82"/>
			<dup/>
			<push arg="114"/>
			<push arg="111"/>
			<push arg="81"/>
			<new/>
			<pcall arg="82"/>
			<dup/>
			<push arg="115"/>
			<push arg="116"/>
			<push arg="91"/>
			<new/>
			<pcall arg="82"/>
			<dup/>
			<push arg="117"/>
			<push arg="116"/>
			<push arg="91"/>
			<new/>
			<pcall arg="82"/>
			<dup/>
			<push arg="118"/>
			<push arg="93"/>
			<push arg="91"/>
			<new/>
			<pcall arg="82"/>
			<dup/>
			<push arg="119"/>
			<push arg="93"/>
			<push arg="91"/>
			<new/>
			<pcall arg="82"/>
			<dup/>
			<push arg="120"/>
			<push arg="93"/>
			<push arg="91"/>
			<new/>
			<pcall arg="82"/>
			<dup/>
			<push arg="121"/>
			<push arg="93"/>
			<push arg="91"/>
			<new/>
			<pcall arg="82"/>
			<dup/>
			<push arg="122"/>
			<push arg="93"/>
			<push arg="91"/>
			<new/>
			<pcall arg="82"/>
			<pusht/>
			<pcall arg="83"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="123" begin="19" end="24"/>
			<lne id="124" begin="25" end="30"/>
			<lne id="125" begin="31" end="36"/>
			<lne id="126" begin="37" end="42"/>
			<lne id="127" begin="43" end="48"/>
			<lne id="128" begin="49" end="54"/>
			<lne id="129" begin="55" end="60"/>
			<lne id="130" begin="61" end="66"/>
			<lne id="131" begin="67" end="72"/>
			<lne id="132" begin="73" end="78"/>
			<lne id="133" begin="79" end="84"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="109" begin="6" end="86"/>
			<lve slot="0" name="17" begin="0" end="87"/>
		</localvariabletable>
	</operation>
	<operation name="134">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="86"/>
		</parameters>
		<code>
			<load arg="19"/>
			<push arg="109"/>
			<call arg="87"/>
			<store arg="29"/>
			<load arg="19"/>
			<push arg="110"/>
			<call arg="88"/>
			<store arg="89"/>
			<load arg="19"/>
			<push arg="112"/>
			<call arg="88"/>
			<store arg="135"/>
			<load arg="19"/>
			<push arg="113"/>
			<call arg="88"/>
			<store arg="136"/>
			<load arg="19"/>
			<push arg="114"/>
			<call arg="88"/>
			<store arg="137"/>
			<load arg="19"/>
			<push arg="115"/>
			<call arg="88"/>
			<store arg="138"/>
			<load arg="19"/>
			<push arg="117"/>
			<call arg="88"/>
			<store arg="139"/>
			<load arg="19"/>
			<push arg="118"/>
			<call arg="88"/>
			<store arg="140"/>
			<load arg="19"/>
			<push arg="119"/>
			<call arg="88"/>
			<store arg="141"/>
			<load arg="19"/>
			<push arg="120"/>
			<call arg="88"/>
			<store arg="142"/>
			<load arg="19"/>
			<push arg="121"/>
			<call arg="88"/>
			<store arg="143"/>
			<load arg="19"/>
			<push arg="122"/>
			<call arg="88"/>
			<store arg="144"/>
			<load arg="89"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="38"/>
			<push arg="145"/>
			<call arg="146"/>
			<call arg="30"/>
			<set arg="38"/>
			<dup/>
			<getasm/>
			<pushi arg="19"/>
			<call arg="30"/>
			<set arg="147"/>
			<pop/>
			<load arg="135"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="38"/>
			<push arg="148"/>
			<call arg="146"/>
			<call arg="30"/>
			<set arg="38"/>
			<pop/>
			<load arg="136"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="38"/>
			<push arg="149"/>
			<call arg="146"/>
			<call arg="30"/>
			<set arg="38"/>
			<pop/>
			<load arg="137"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="38"/>
			<push arg="150"/>
			<call arg="146"/>
			<call arg="30"/>
			<set arg="38"/>
			<pop/>
			<load arg="138"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="38"/>
			<push arg="151"/>
			<call arg="146"/>
			<call arg="30"/>
			<set arg="38"/>
			<pop/>
			<load arg="139"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="38"/>
			<push arg="152"/>
			<call arg="146"/>
			<call arg="30"/>
			<set arg="38"/>
			<pop/>
			<load arg="140"/>
			<dup/>
			<getasm/>
			<pushi arg="19"/>
			<call arg="30"/>
			<set arg="147"/>
			<dup/>
			<getasm/>
			<load arg="89"/>
			<call arg="30"/>
			<set arg="153"/>
			<dup/>
			<getasm/>
			<load arg="138"/>
			<call arg="30"/>
			<set arg="154"/>
			<pop/>
			<load arg="141"/>
			<dup/>
			<getasm/>
			<pushi arg="19"/>
			<call arg="30"/>
			<set arg="147"/>
			<dup/>
			<getasm/>
			<load arg="138"/>
			<call arg="30"/>
			<set arg="153"/>
			<dup/>
			<getasm/>
			<load arg="135"/>
			<call arg="30"/>
			<set arg="154"/>
			<pop/>
			<load arg="142"/>
			<dup/>
			<getasm/>
			<pushi arg="19"/>
			<call arg="30"/>
			<set arg="147"/>
			<dup/>
			<getasm/>
			<load arg="138"/>
			<call arg="30"/>
			<set arg="153"/>
			<dup/>
			<getasm/>
			<load arg="136"/>
			<call arg="30"/>
			<set arg="154"/>
			<pop/>
			<load arg="143"/>
			<dup/>
			<getasm/>
			<pushi arg="19"/>
			<call arg="30"/>
			<set arg="147"/>
			<dup/>
			<getasm/>
			<load arg="136"/>
			<call arg="30"/>
			<set arg="153"/>
			<dup/>
			<getasm/>
			<load arg="139"/>
			<call arg="30"/>
			<set arg="154"/>
			<pop/>
			<load arg="144"/>
			<dup/>
			<getasm/>
			<pushi arg="19"/>
			<call arg="30"/>
			<set arg="147"/>
			<dup/>
			<getasm/>
			<load arg="139"/>
			<call arg="30"/>
			<set arg="153"/>
			<dup/>
			<getasm/>
			<load arg="137"/>
			<call arg="30"/>
			<set arg="154"/>
			<pop/>
		</code>
		<linenumbertable>
			<lne id="155" begin="51" end="51"/>
			<lne id="156" begin="51" end="52"/>
			<lne id="157" begin="53" end="53"/>
			<lne id="158" begin="51" end="54"/>
			<lne id="159" begin="49" end="56"/>
			<lne id="160" begin="59" end="59"/>
			<lne id="161" begin="57" end="61"/>
			<lne id="123" begin="48" end="62"/>
			<lne id="162" begin="66" end="66"/>
			<lne id="163" begin="66" end="67"/>
			<lne id="164" begin="68" end="68"/>
			<lne id="165" begin="66" end="69"/>
			<lne id="166" begin="64" end="71"/>
			<lne id="124" begin="63" end="72"/>
			<lne id="167" begin="76" end="76"/>
			<lne id="168" begin="76" end="77"/>
			<lne id="169" begin="78" end="78"/>
			<lne id="170" begin="76" end="79"/>
			<lne id="171" begin="74" end="81"/>
			<lne id="125" begin="73" end="82"/>
			<lne id="172" begin="86" end="86"/>
			<lne id="173" begin="86" end="87"/>
			<lne id="174" begin="88" end="88"/>
			<lne id="175" begin="86" end="89"/>
			<lne id="176" begin="84" end="91"/>
			<lne id="126" begin="83" end="92"/>
			<lne id="177" begin="96" end="96"/>
			<lne id="178" begin="96" end="97"/>
			<lne id="179" begin="98" end="98"/>
			<lne id="180" begin="96" end="99"/>
			<lne id="181" begin="94" end="101"/>
			<lne id="127" begin="93" end="102"/>
			<lne id="182" begin="106" end="106"/>
			<lne id="183" begin="106" end="107"/>
			<lne id="184" begin="108" end="108"/>
			<lne id="185" begin="106" end="109"/>
			<lne id="186" begin="104" end="111"/>
			<lne id="128" begin="103" end="112"/>
			<lne id="187" begin="116" end="116"/>
			<lne id="188" begin="114" end="118"/>
			<lne id="189" begin="121" end="121"/>
			<lne id="190" begin="119" end="123"/>
			<lne id="191" begin="126" end="126"/>
			<lne id="192" begin="124" end="128"/>
			<lne id="129" begin="113" end="129"/>
			<lne id="193" begin="133" end="133"/>
			<lne id="194" begin="131" end="135"/>
			<lne id="195" begin="138" end="138"/>
			<lne id="196" begin="136" end="140"/>
			<lne id="197" begin="143" end="143"/>
			<lne id="198" begin="141" end="145"/>
			<lne id="130" begin="130" end="146"/>
			<lne id="199" begin="150" end="150"/>
			<lne id="200" begin="148" end="152"/>
			<lne id="201" begin="155" end="155"/>
			<lne id="202" begin="153" end="157"/>
			<lne id="203" begin="160" end="160"/>
			<lne id="204" begin="158" end="162"/>
			<lne id="131" begin="147" end="163"/>
			<lne id="205" begin="167" end="167"/>
			<lne id="206" begin="165" end="169"/>
			<lne id="207" begin="172" end="172"/>
			<lne id="208" begin="170" end="174"/>
			<lne id="209" begin="177" end="177"/>
			<lne id="210" begin="175" end="179"/>
			<lne id="132" begin="164" end="180"/>
			<lne id="211" begin="184" end="184"/>
			<lne id="212" begin="182" end="186"/>
			<lne id="213" begin="189" end="189"/>
			<lne id="214" begin="187" end="191"/>
			<lne id="215" begin="194" end="194"/>
			<lne id="216" begin="192" end="196"/>
			<lne id="133" begin="181" end="197"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="3" name="110" begin="7" end="197"/>
			<lve slot="4" name="112" begin="11" end="197"/>
			<lve slot="5" name="113" begin="15" end="197"/>
			<lve slot="6" name="114" begin="19" end="197"/>
			<lve slot="7" name="115" begin="23" end="197"/>
			<lve slot="8" name="117" begin="27" end="197"/>
			<lve slot="9" name="118" begin="31" end="197"/>
			<lve slot="10" name="119" begin="35" end="197"/>
			<lve slot="11" name="120" begin="39" end="197"/>
			<lve slot="12" name="121" begin="43" end="197"/>
			<lve slot="13" name="122" begin="47" end="197"/>
			<lve slot="2" name="109" begin="3" end="197"/>
			<lve slot="0" name="17" begin="0" end="197"/>
			<lve slot="1" name="106" begin="0" end="197"/>
		</localvariabletable>
	</operation>
	<operation name="217">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<push arg="218"/>
			<push arg="54"/>
			<findme/>
			<push arg="74"/>
			<call arg="75"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<get arg="1"/>
			<push arg="76"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="49"/>
			<pcall arg="77"/>
			<dup/>
			<push arg="219"/>
			<load arg="19"/>
			<pcall arg="78"/>
			<dup/>
			<push arg="220"/>
			<push arg="93"/>
			<push arg="91"/>
			<new/>
			<pcall arg="82"/>
			<pusht/>
			<pcall arg="83"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="221" begin="19" end="24"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="219" begin="6" end="26"/>
			<lve slot="0" name="17" begin="0" end="27"/>
		</localvariabletable>
	</operation>
	<operation name="222">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="86"/>
		</parameters>
		<code>
			<load arg="19"/>
			<push arg="219"/>
			<call arg="87"/>
			<store arg="29"/>
			<load arg="19"/>
			<push arg="220"/>
			<call arg="88"/>
			<store arg="89"/>
			<load arg="89"/>
			<dup/>
			<getasm/>
			<pushi arg="19"/>
			<call arg="30"/>
			<set arg="147"/>
			<dup/>
			<getasm/>
			<getasm/>
			<load arg="29"/>
			<get arg="223"/>
			<load arg="29"/>
			<get arg="224"/>
			<push arg="225"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="226"/>
			<set arg="38"/>
			<call arg="227"/>
			<load arg="29"/>
			<get arg="224"/>
			<push arg="225"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="228"/>
			<set arg="38"/>
			<call arg="227"/>
			<call arg="229"/>
			<if arg="230"/>
			<push arg="113"/>
			<goto arg="231"/>
			<push arg="114"/>
			<call arg="232"/>
			<call arg="30"/>
			<set arg="153"/>
			<dup/>
			<getasm/>
			<getasm/>
			<load arg="29"/>
			<get arg="233"/>
			<load arg="29"/>
			<get arg="224"/>
			<push arg="225"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="228"/>
			<set arg="38"/>
			<call arg="227"/>
			<load arg="29"/>
			<get arg="224"/>
			<push arg="225"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="234"/>
			<set arg="38"/>
			<call arg="227"/>
			<call arg="229"/>
			<if arg="235"/>
			<push arg="117"/>
			<goto arg="236"/>
			<push arg="115"/>
			<call arg="232"/>
			<call arg="30"/>
			<set arg="154"/>
			<pop/>
		</code>
		<linenumbertable>
			<lne id="237" begin="11" end="11"/>
			<lne id="238" begin="9" end="13"/>
			<lne id="239" begin="16" end="16"/>
			<lne id="240" begin="17" end="17"/>
			<lne id="241" begin="17" end="18"/>
			<lne id="242" begin="19" end="19"/>
			<lne id="243" begin="19" end="20"/>
			<lne id="244" begin="21" end="26"/>
			<lne id="245" begin="19" end="27"/>
			<lne id="246" begin="28" end="28"/>
			<lne id="247" begin="28" end="29"/>
			<lne id="248" begin="30" end="35"/>
			<lne id="249" begin="28" end="36"/>
			<lne id="250" begin="19" end="37"/>
			<lne id="251" begin="39" end="39"/>
			<lne id="252" begin="41" end="41"/>
			<lne id="253" begin="19" end="41"/>
			<lne id="254" begin="16" end="42"/>
			<lne id="255" begin="14" end="44"/>
			<lne id="256" begin="47" end="47"/>
			<lne id="257" begin="48" end="48"/>
			<lne id="258" begin="48" end="49"/>
			<lne id="259" begin="50" end="50"/>
			<lne id="260" begin="50" end="51"/>
			<lne id="261" begin="52" end="57"/>
			<lne id="262" begin="50" end="58"/>
			<lne id="263" begin="59" end="59"/>
			<lne id="264" begin="59" end="60"/>
			<lne id="265" begin="61" end="66"/>
			<lne id="266" begin="59" end="67"/>
			<lne id="267" begin="50" end="68"/>
			<lne id="268" begin="70" end="70"/>
			<lne id="269" begin="72" end="72"/>
			<lne id="270" begin="50" end="72"/>
			<lne id="271" begin="47" end="73"/>
			<lne id="272" begin="45" end="75"/>
			<lne id="221" begin="8" end="76"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="3" name="220" begin="7" end="76"/>
			<lve slot="2" name="219" begin="3" end="76"/>
			<lve slot="0" name="17" begin="0" end="76"/>
			<lve slot="1" name="106" begin="0" end="76"/>
		</localvariabletable>
	</operation>
</asm>
