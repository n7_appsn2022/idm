/**
 */
package simplepdl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link simplepdl.Requirement#getQuantity_required <em>Quantity required</em>}</li>
 *   <li>{@link simplepdl.Requirement#getIs_demanded <em>Is demanded</em>}</li>
 *   <li>{@link simplepdl.Requirement#getRequire <em>Require</em>}</li>
 * </ul>
 *
 * @see simplepdl.SimplepdlPackage#getRequirement()
 * @model
 * @generated
 */
public interface Requirement extends EObject {
	/**
	 * Returns the value of the '<em><b>Quantity required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity required</em>' attribute.
	 * @see #setQuantity_required(int)
	 * @see simplepdl.SimplepdlPackage#getRequirement_Quantity_required()
	 * @model required="true"
	 * @generated
	 */
	int getQuantity_required();

	/**
	 * Sets the value of the '{@link simplepdl.Requirement#getQuantity_required <em>Quantity required</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity required</em>' attribute.
	 * @see #getQuantity_required()
	 * @generated
	 */
	void setQuantity_required(int value);

	/**
	 * Returns the value of the '<em><b>Is demanded</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link simplepdl.WorkDefinition#getDemand <em>Demand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is demanded</em>' container reference.
	 * @see #setIs_demanded(WorkDefinition)
	 * @see simplepdl.SimplepdlPackage#getRequirement_Is_demanded()
	 * @see simplepdl.WorkDefinition#getDemand
	 * @model opposite="demand" transient="false"
	 * @generated
	 */
	WorkDefinition getIs_demanded();

	/**
	 * Sets the value of the '{@link simplepdl.Requirement#getIs_demanded <em>Is demanded</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is demanded</em>' container reference.
	 * @see #getIs_demanded()
	 * @generated
	 */
	void setIs_demanded(WorkDefinition value);

	/**
	 * Returns the value of the '<em><b>Require</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link simplepdl.Resources#getIs_required <em>Is required</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Require</em>' reference.
	 * @see #setRequire(Resources)
	 * @see simplepdl.SimplepdlPackage#getRequirement_Require()
	 * @see simplepdl.Resources#getIs_required
	 * @model opposite="is_required" required="true"
	 * @generated
	 */
	Resources getRequire();

	/**
	 * Sets the value of the '{@link simplepdl.Requirement#getRequire <em>Require</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Require</em>' reference.
	 * @see #getRequire()
	 * @generated
	 */
	void setRequire(Resources value);

} // Requirement
