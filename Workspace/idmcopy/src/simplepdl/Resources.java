/**
 */
package simplepdl;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resources</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link simplepdl.Resources#getName <em>Name</em>}</li>
 *   <li>{@link simplepdl.Resources#getAvailable_quantity <em>Available quantity</em>}</li>
 *   <li>{@link simplepdl.Resources#getIs_required <em>Is required</em>}</li>
 * </ul>
 *
 * @see simplepdl.SimplepdlPackage#getResources()
 * @model
 * @generated
 */
public interface Resources extends ProcessElement {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see simplepdl.SimplepdlPackage#getResources_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link simplepdl.Resources#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Available quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Available quantity</em>' attribute.
	 * @see #setAvailable_quantity(int)
	 * @see simplepdl.SimplepdlPackage#getResources_Available_quantity()
	 * @model required="true"
	 * @generated
	 */
	int getAvailable_quantity();

	/**
	 * Sets the value of the '{@link simplepdl.Resources#getAvailable_quantity <em>Available quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Available quantity</em>' attribute.
	 * @see #getAvailable_quantity()
	 * @generated
	 */
	void setAvailable_quantity(int value);

	/**
	 * Returns the value of the '<em><b>Is required</b></em>' reference list.
	 * The list contents are of type {@link simplepdl.Requirement}.
	 * It is bidirectional and its opposite is '{@link simplepdl.Requirement#getRequire <em>Require</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is required</em>' reference list.
	 * @see simplepdl.SimplepdlPackage#getResources_Is_required()
	 * @see simplepdl.Requirement#getRequire
	 * @model opposite="require"
	 * @generated
	 */
	EList<Requirement> getIs_required();

} // Resources
