/**
 */
package simplepdl.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import simplepdl.Requirement;
import simplepdl.Resources;
import simplepdl.SimplepdlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resources</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link simplepdl.impl.ResourcesImpl#getName <em>Name</em>}</li>
 *   <li>{@link simplepdl.impl.ResourcesImpl#getAvailable_quantity <em>Available quantity</em>}</li>
 *   <li>{@link simplepdl.impl.ResourcesImpl#getIs_required <em>Is required</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResourcesImpl extends ProcessElementImpl implements Resources {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getAvailable_quantity() <em>Available quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvailable_quantity()
	 * @generated
	 * @ordered
	 */
	protected static final int AVAILABLE_QUANTITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getAvailable_quantity() <em>Available quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvailable_quantity()
	 * @generated
	 * @ordered
	 */
	protected int available_quantity = AVAILABLE_QUANTITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getIs_required() <em>Is required</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIs_required()
	 * @generated
	 * @ordered
	 */
	protected EList<Requirement> is_required;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResourcesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SimplepdlPackage.Literals.RESOURCES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SimplepdlPackage.RESOURCES__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAvailable_quantity() {
		return available_quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAvailable_quantity(int newAvailable_quantity) {
		int oldAvailable_quantity = available_quantity;
		available_quantity = newAvailable_quantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SimplepdlPackage.RESOURCES__AVAILABLE_QUANTITY, oldAvailable_quantity, available_quantity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Requirement> getIs_required() {
		if (is_required == null) {
			is_required = new EObjectWithInverseResolvingEList<Requirement>(Requirement.class, this, SimplepdlPackage.RESOURCES__IS_REQUIRED, SimplepdlPackage.REQUIREMENT__REQUIRE);
		}
		return is_required;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SimplepdlPackage.RESOURCES__IS_REQUIRED:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getIs_required()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SimplepdlPackage.RESOURCES__IS_REQUIRED:
				return ((InternalEList<?>)getIs_required()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SimplepdlPackage.RESOURCES__NAME:
				return getName();
			case SimplepdlPackage.RESOURCES__AVAILABLE_QUANTITY:
				return getAvailable_quantity();
			case SimplepdlPackage.RESOURCES__IS_REQUIRED:
				return getIs_required();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SimplepdlPackage.RESOURCES__NAME:
				setName((String)newValue);
				return;
			case SimplepdlPackage.RESOURCES__AVAILABLE_QUANTITY:
				setAvailable_quantity((Integer)newValue);
				return;
			case SimplepdlPackage.RESOURCES__IS_REQUIRED:
				getIs_required().clear();
				getIs_required().addAll((Collection<? extends Requirement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SimplepdlPackage.RESOURCES__NAME:
				setName(NAME_EDEFAULT);
				return;
			case SimplepdlPackage.RESOURCES__AVAILABLE_QUANTITY:
				setAvailable_quantity(AVAILABLE_QUANTITY_EDEFAULT);
				return;
			case SimplepdlPackage.RESOURCES__IS_REQUIRED:
				getIs_required().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SimplepdlPackage.RESOURCES__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case SimplepdlPackage.RESOURCES__AVAILABLE_QUANTITY:
				return available_quantity != AVAILABLE_QUANTITY_EDEFAULT;
			case SimplepdlPackage.RESOURCES__IS_REQUIRED:
				return is_required != null && !is_required.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", available_quantity: ");
		result.append(available_quantity);
		result.append(')');
		return result.toString();
	}

} //ResourcesImpl
