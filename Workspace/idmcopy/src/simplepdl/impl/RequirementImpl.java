/**
 */
package simplepdl.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import simplepdl.Requirement;
import simplepdl.Resources;
import simplepdl.SimplepdlPackage;
import simplepdl.WorkDefinition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Requirement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link simplepdl.impl.RequirementImpl#getQuantity_required <em>Quantity required</em>}</li>
 *   <li>{@link simplepdl.impl.RequirementImpl#getIs_demanded <em>Is demanded</em>}</li>
 *   <li>{@link simplepdl.impl.RequirementImpl#getRequire <em>Require</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RequirementImpl extends MinimalEObjectImpl.Container implements Requirement {
	/**
	 * The default value of the '{@link #getQuantity_required() <em>Quantity required</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity_required()
	 * @generated
	 * @ordered
	 */
	protected static final int QUANTITY_REQUIRED_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getQuantity_required() <em>Quantity required</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantity_required()
	 * @generated
	 * @ordered
	 */
	protected int quantity_required = QUANTITY_REQUIRED_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRequire() <em>Require</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequire()
	 * @generated
	 * @ordered
	 */
	protected Resources require;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SimplepdlPackage.Literals.REQUIREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getQuantity_required() {
		return quantity_required;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantity_required(int newQuantity_required) {
		int oldQuantity_required = quantity_required;
		quantity_required = newQuantity_required;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SimplepdlPackage.REQUIREMENT__QUANTITY_REQUIRED, oldQuantity_required, quantity_required));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkDefinition getIs_demanded() {
		if (eContainerFeatureID() != SimplepdlPackage.REQUIREMENT__IS_DEMANDED) return null;
		return (WorkDefinition)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIs_demanded(WorkDefinition newIs_demanded, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newIs_demanded, SimplepdlPackage.REQUIREMENT__IS_DEMANDED, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIs_demanded(WorkDefinition newIs_demanded) {
		if (newIs_demanded != eInternalContainer() || (eContainerFeatureID() != SimplepdlPackage.REQUIREMENT__IS_DEMANDED && newIs_demanded != null)) {
			if (EcoreUtil.isAncestor(this, newIs_demanded))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newIs_demanded != null)
				msgs = ((InternalEObject)newIs_demanded).eInverseAdd(this, SimplepdlPackage.WORK_DEFINITION__DEMAND, WorkDefinition.class, msgs);
			msgs = basicSetIs_demanded(newIs_demanded, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SimplepdlPackage.REQUIREMENT__IS_DEMANDED, newIs_demanded, newIs_demanded));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resources getRequire() {
		if (require != null && require.eIsProxy()) {
			InternalEObject oldRequire = (InternalEObject)require;
			require = (Resources)eResolveProxy(oldRequire);
			if (require != oldRequire) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SimplepdlPackage.REQUIREMENT__REQUIRE, oldRequire, require));
			}
		}
		return require;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resources basicGetRequire() {
		return require;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRequire(Resources newRequire, NotificationChain msgs) {
		Resources oldRequire = require;
		require = newRequire;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SimplepdlPackage.REQUIREMENT__REQUIRE, oldRequire, newRequire);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequire(Resources newRequire) {
		if (newRequire != require) {
			NotificationChain msgs = null;
			if (require != null)
				msgs = ((InternalEObject)require).eInverseRemove(this, SimplepdlPackage.RESOURCES__IS_REQUIRED, Resources.class, msgs);
			if (newRequire != null)
				msgs = ((InternalEObject)newRequire).eInverseAdd(this, SimplepdlPackage.RESOURCES__IS_REQUIRED, Resources.class, msgs);
			msgs = basicSetRequire(newRequire, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SimplepdlPackage.REQUIREMENT__REQUIRE, newRequire, newRequire));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SimplepdlPackage.REQUIREMENT__IS_DEMANDED:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetIs_demanded((WorkDefinition)otherEnd, msgs);
			case SimplepdlPackage.REQUIREMENT__REQUIRE:
				if (require != null)
					msgs = ((InternalEObject)require).eInverseRemove(this, SimplepdlPackage.RESOURCES__IS_REQUIRED, Resources.class, msgs);
				return basicSetRequire((Resources)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SimplepdlPackage.REQUIREMENT__IS_DEMANDED:
				return basicSetIs_demanded(null, msgs);
			case SimplepdlPackage.REQUIREMENT__REQUIRE:
				return basicSetRequire(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case SimplepdlPackage.REQUIREMENT__IS_DEMANDED:
				return eInternalContainer().eInverseRemove(this, SimplepdlPackage.WORK_DEFINITION__DEMAND, WorkDefinition.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SimplepdlPackage.REQUIREMENT__QUANTITY_REQUIRED:
				return getQuantity_required();
			case SimplepdlPackage.REQUIREMENT__IS_DEMANDED:
				return getIs_demanded();
			case SimplepdlPackage.REQUIREMENT__REQUIRE:
				if (resolve) return getRequire();
				return basicGetRequire();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SimplepdlPackage.REQUIREMENT__QUANTITY_REQUIRED:
				setQuantity_required((Integer)newValue);
				return;
			case SimplepdlPackage.REQUIREMENT__IS_DEMANDED:
				setIs_demanded((WorkDefinition)newValue);
				return;
			case SimplepdlPackage.REQUIREMENT__REQUIRE:
				setRequire((Resources)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SimplepdlPackage.REQUIREMENT__QUANTITY_REQUIRED:
				setQuantity_required(QUANTITY_REQUIRED_EDEFAULT);
				return;
			case SimplepdlPackage.REQUIREMENT__IS_DEMANDED:
				setIs_demanded((WorkDefinition)null);
				return;
			case SimplepdlPackage.REQUIREMENT__REQUIRE:
				setRequire((Resources)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SimplepdlPackage.REQUIREMENT__QUANTITY_REQUIRED:
				return quantity_required != QUANTITY_REQUIRED_EDEFAULT;
			case SimplepdlPackage.REQUIREMENT__IS_DEMANDED:
				return getIs_demanded() != null;
			case SimplepdlPackage.REQUIREMENT__REQUIRE:
				return require != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (quantity_required: ");
		result.append(quantity_required);
		result.append(')');
		return result.toString();
	}

} //RequirementImpl
